<?php
/*
Instrutions wsdl:
    For generate the .wsdl put the next url in the browser 
    and save this in a file <file_name>.wsdl
    http://<name_host>/reto_4/server_soap.php?wsdl
*/

require_once "../lib/nusoap/nusoap.php";
 
function getMultiplication($var_1,$var_2) {
    if(isset($var_1, $var_2)){
        return join(",", array($var_1 * $var_2));
    }else{
        return "No products listed under that category";
    }
}
 
function getSum($var_1,$var_2) {
    if(isset($var_1, $var_2)){
        return join(",", array($var_1 + $var_2));
    }else{
        return "No products listed under that category";
    }
}

$server = new soap_server();
/*
//Example SOAP without wsdl
$server->register("getSubtraction");
$server->register("getSum");
$server->service($HTTP_RAW_POST_DATA);
*/

$server->configureWSDL("server_soap", "urn:server_soap");
 
$server->register("getMultiplication",
    array("var_1" => "xsd:string", "var_2" => "xsd:string"),
    array("return" => "xsd:string"),
    "urn:server_soap",
    "urn:server_soap#getMultiplication",
    "rpc",
    "encoded",
    "Multiplies var_1 and var_2 and return the result");

$server->register("getSum",
    array("var_1" => "xsd:string", "var_2" => "xsd:string"),
    array("return" => "xsd:string"),
    "urn:server_soap",
    "urn:server_soap#getSum",
    "rpc",
    "encoded",
    "Sum var_1 and var_2 and return the result");

$server->service($HTTP_RAW_POST_DATA);
?>