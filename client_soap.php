<?php
  require_once "../lib/nusoap/nusoap.php";

  function call_SOAP_ws($var_1=0, $var_2=0, $sim, $soap_server){
    /*
    //Example who to call the web service SOAP without the wsdl
    $client = new nusoap_client("http://localhost/reto_4/server_soap.php");
    */
    $client = new nusoap_client($soap_server ,true);
    $error = $client->getError();
    if ($error) {
        echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
    }

    $ws_method = '';
    switch ($sim) {
      case '+':
        $ws_method = "getSum";  
        break;
      case '*':
        $ws_method ="getMultiplication";
        break;
    }
    
    $result = $client->call($ws_method, array("var_1" => $var_1, "var_2" => $var_2));  
    return $result;
    /*
    if ($client->fault) {
        echo "<h2>Fault</h2><pre>";
        print_r($result);
        echo "</pre>";
    }
    else {
        $error = $client->getError();
        if ($error) {
            echo "<h2>Error</h2><pre>" . $error . "</pre>";
        }
        else {
            echo "<h2>Result:</h2><pre>";
            echo '<p>'.$var_1.$sim.$var_2.'</p>';
            echo '<p>'.$result.'</p>';
            echo "</pre>";
        }
    }
    */
  }
?>