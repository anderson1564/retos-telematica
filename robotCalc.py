#!/usr/bin/env python

#Libreria SOAP : python-suds

import sys
import urllib2
import json
from xml.dom import minidom
from suds.client import Client

xmlFile = 'calc.xml'
xmlTag = 'webservices'
arr_url = []
arr_method = []
matrix_params = []
arr_response = [] 

#### Read the xml
xml_documento = minidom.parse(xmlFile)
nodos = xml_documento.childNodes

lista = nodos[0].getElementsByTagName("webservice")
for nodo in lista:
 	lista_aux = nodo.getElementsByTagName("url")
 	for x in lista_aux:
 		text = x.childNodes[0].data.encode("utf8","ignore")
 		arr_url.append(text)
 	lista_aux = nodo.getElementsByTagName("method")
 	for x in lista_aux:
 		text = x.childNodes[0].data.encode("utf8","ignore")
 		arr_method.append(text)
 	lista_aux = nodo.getElementsByTagName("params")
 	for x in lista_aux:
 		a = x.getElementsByTagName("p1")
 		text_1 = a[0].childNodes[0].data.encode("utf8","ignore")
 		a = x.getElementsByTagName("p2")
 		text_2 = a[0].childNodes[0].data.encode("utf8","ignore")
 		matrix_params.append([text_1,text_2])

#### Call the webservices

for i in range(len(arr_url)):
	url =  arr_url[i]
	method =  arr_method[i]
	var_1 = matrix_params[i][0]
	var_2 = matrix_params[i][1]
	response = 'Problem with the webservices'
#################### SOAP Call ############################
	if(method == 'multiplicar'):
		client = Client(url)
		response = client.service.getMultiplication(var_1, var_2)
	if(method == 'sumar'):
		client = Client(url)
		response = client.service.getSum(var_1, var_2)	
#################### REST Call ############################
	if(method == 'restar'):
		op = '-'
		result = urllib2.urlopen(url + "?op=" + op + "&var_1=" + var_1 + "&var_2=" + var_2, " ")
		jason = json.load(result)
		response = jason[op]
	if(method == 'dividir'):
		op = '/'		
		result = urllib2.urlopen(url + "?op=" + op + "&var_1=" + var_1 + "&var_2=" + var_2, " ")
		jason = json.load(result)
		response = jason[op]
	arr_response.append(response)

#### Change the results in the xml.dom

i= 0
for node in lista:
	a = node.getElementsByTagName('result')[0].childNodes[0].nodeValue = arr_response[i]
	i = i+1

#### Rewriting the xml 

f = open(xmlFile, "w")
xml_documento.writexml(f)
f.close()