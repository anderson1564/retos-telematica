<!DOCTYPE html>
<html>
  <head>
    <title>Reto 4</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="../bootstrap-3.0.0/dist/css/bootstrap.min.css" rel="stylesheet" media="screen">
  </head>
  <body>
    <nav class="navbar navbar-inverse" role="navigation">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header navbar-static-top">
        <a class="navbar-brand" href="#">Reto 4</a>
      </div>
    </nav>
    
    <div class="container">
      <form class="form-horizontal" role="form" action="<?php $_PHP_SELF ?>" method="POST">
        <div>
          <label class="control-label">Variable 1</label>
          <div>
            <input type="text" name="var_1" class="form-control" placeholder="Number, Example 15">
          </div>
        </div>
        <div>
          <label class="control-label">Variable 2</label>
          <div>
            <input type="text" name="var_2" class="form-control" placeholder="Number, Example 15">
          </div>
        </div>
        <hr>
        <div class="btn-group">
          <button type="submit" class="btn btn-primary" name="plus" value="+">Sumar</button>
          <button type="submit" class="btn btn-primary" name="mul" value="*">Multiplicar</button>
          <button type="submit" class="btn btn-success" name="minus" value="-">Restar</button>
          <button type="submit" class="btn btn-success" name="divided" value="/">Dividir</button>
        </div>
      </form>

      <?php 
        include 'client_soap.php';
        include 'client_rest.php';

        if(isset($_POST['var_1'],$_POST['var_2'])){
          
          $xml=simplexml_load_file("calcserver.xml");

          $server_soap = $xml->url[0]."?wsdl";
          $server_rest = $xml->url[1];
          
          $return=0;
          $sim ='';
          $var_1 = $_POST['var_1'];  
          $var_2 = $_POST['var_2']; 
          
          if(isset($_POST['plus'])){
            $sim = $_POST['plus'];
            $return = call_SOAP_ws($var_1, $var_2, $sim, $server_soap);
          }else if(isset($_POST['mul'])){
            $sim = $_POST['mul'];
            $return = call_SOAP_ws($var_1, $var_2, $sim, $server_soap);
          }else if(isset($_POST['minus'])){
            $sim = $_POST['minus'];
            $return = call_rest_ws($var_1, $var_2, $sim, $server_rest);
          }else if(isset($_POST['divided'])){
            $sim = $_POST['divided'];
            $return = call_rest_ws($var_1, $var_2, $sim, $server_rest);
          }

          echo '<h2>Result:</h2><pre>';
          echo '<p>'.$var_1.$sim.$var_2.'</p>';
          echo '<p>'.$return.'</p>';
          echo "</pre>";
        }
      ?>
    </div>
    
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    
    <div id="footer">
      <div class="container">
        <p class="text-muted credit">Anderson LM, 2013.</p>
      </div>
    </div>

    <!-- jQuery -->
    <script src="//code.jquery.com/jquery.js"></script>
    <!-- Boostrap -->
    <script src="../bootstrap-3.0.0/assets/js/jquery.js"></script>
    <script src="../bootstrap-3.0.0/dist/js/bootstrap.min.js"></script> 
    <script src="../bootstrap-3.0.0/assets/js/holder.js"></script>
  </body>
</html>